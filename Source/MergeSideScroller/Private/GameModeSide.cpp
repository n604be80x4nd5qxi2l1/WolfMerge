// Fill out your copyright notice in the Description page of Project Settings.


#include "GameModeSide.h"
#include "Types.h"
#include "Kismet/GameplayStatics.h"

void AGameModeSide::BeginPlay()
{
	GetWorld()->Exec(GetWorld(), TEXT("DisableAllScreenMessages"));

	GetOptionValue(TEXT("FirstSide")) == "True" ? IsFirstSide = true : IsFirstSide = false;
	GetOptionValue(TEXT("LastSide")) == "True" ? IsLastSide = true : IsLastSide = false;


	FString LevelsNumString = GetOptionValue(TEXT("LevelPartsInLevel"));

	uint32 TempLevelsNum = FCString::Atoi(const_cast<TCHAR*>(*LevelsNumString));
	if (TempLevelsNum > 255 || TempLevelsNum < 0)
		TempLevelsNum = 0;
	
	const uint8 LevelPartsAmount = TempLevelsNum;
	const FName RowName{GetOptionValue(TEXT("Biom"))};
	const FString Context;
	const bool IsRowExists = false;
	
	CurrentLevelSettings = LevelSettingsDataTable->FindRow<FSideLevelSettings>(RowName, Context, IsRowExists);

	
	LevelBuilder = GetWorld()->SpawnActorDeferred<ALevelBuilder>(LevelBuilderClass, FTransform());
	LevelBuilder->Initialize(LevelPartsAmount, *CurrentLevelSettings);
	LevelBuilder->FinishSpawning(FTransform(FVector(0,0,0)));
	
}

FString AGameModeSide::GetOptionValue(const FString& Key) const
{
	return UGameplayStatics::ParseOption(OptionsString, Key);	
};


