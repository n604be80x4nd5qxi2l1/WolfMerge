// Fill out your copyright notice in the Description page of Project Settings.


#include "Side/LevelPartBasic.h"

// Sets default values
ALevelPartBasic::ALevelPartBasic()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EnterVolume = nullptr;
	FirstHalfVolume = nullptr;
	CenterVolume = nullptr;
	SecondHalfVolume = nullptr;
	ExitVolume = nullptr;

}

// Called when the game starts or when spawned
void ALevelPartBasic::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALevelPartBasic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

