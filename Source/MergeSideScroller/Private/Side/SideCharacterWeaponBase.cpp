// Fill out your copyright notice in the Description page of Project Settings.


#include "Side/SideCharacterWeaponBase.h"

// Sets default values
ASideCharacterWeaponBase::ASideCharacterWeaponBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	RootComponent = Root;

	WeaponSkeletalMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Weapon Skeletal Mesh"));
	WeaponSkeletalMesh->SetGenerateOverlapEvents(false);
	WeaponSkeletalMesh->SetCollisionProfileName(TEXT("NoCollision"));
	WeaponSkeletalMesh->SetupAttachment(RootComponent);

	WeaponStaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Weapon Static Mesh"));
	WeaponStaticMesh->SetGenerateOverlapEvents(false);
	WeaponStaticMesh->SetCollisionProfileName(TEXT("NoCollision"));
	WeaponStaticMesh->SetupAttachment(RootComponent);

	WeaponCommonAura = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Weapon Common Aura"));
	WeaponCommonAura->SetupAttachment(RootComponent);
	WeaponElementalAura = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Weapon Elemental Aura"));
	WeaponElementalAura->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ASideCharacterWeaponBase::BeginPlay()
{
	Super::BeginPlay();

	if (WeaponSkeletalMesh && !WeaponSkeletalMesh->SkeletalMesh)
	{
		WeaponSkeletalMesh->DestroyComponent();
	}

	if (WeaponStaticMesh && !WeaponStaticMesh->GetStaticMesh())
	{
		WeaponStaticMesh->DestroyComponent();
	}

}

// Called every frame
void ASideCharacterWeaponBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

