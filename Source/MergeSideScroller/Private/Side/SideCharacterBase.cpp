// Fill out your copyright notice in the Description page of Project Settings.


#include "Side/SideCharacterBase.h"

// Sets default values
ASideCharacterBase::ASideCharacterBase()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	CharacterWeapon = nullptr;
	CharacterLevel = 1;
	BasicDamage = 1.0f;
	AttackSpeed = 1.0f;
	
	CharacterParameters = FCharacterParameters();
	
	HealthComponent = CreateDefaultSubobject<USideCharacterHealthComponent>(TEXT("Character Health Component"));

	MainWeaponSocketName = "RightHandWeaponSocket";
	SecondaryWeaponSocketName = "LeftHandWeaponSocket";
	
	/*CharacterAuraBasicComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Character Aura Basic"));
	CharacterAuraElementComponent = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("Character Aura Element"));
	CharacterAuraBasicComponent->SetupAttachment(RootComponent);
	CharacterAuraElementComponent->SetupAttachment(RootComponent);*/
}

// Called when the game starts or when spawned
void ASideCharacterBase::BeginPlay()
{
	Super::BeginPlay();


	
	CharacterWeapon->AttachToComponent(
		GetMesh(),
		FAttachmentTransformRules::SnapToTargetNotIncludingScale,
		MainWeaponSocketName);	
}

// Called every frame
void ASideCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASideCharacterBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ASideCharacterBase::Initialize(const FCharacterParameters InCharacterParameters, const bool InIsEnemy)
{
	//CharacterParameters = InCharacterParameters;
	bIsEnemy = InIsEnemy;

	if (CharacterWeaponClass->IsValidLowLevelFast())
	{
		CharacterWeapon = GetWorld()->SpawnActor<ASideCharacterWeaponBase>(CharacterWeaponClass);
	}
	
	if(CharacterParameters.RightHanded)
	{
		MainWeaponSocketName = "RightHandWeaponSocket";
		SecondaryWeaponSocketName = "LeftHandWeaponSocket";
	}
	else
	{
		MainWeaponSocketName = "LeftHandWeaponSocket";
		SecondaryWeaponSocketName = "RightHandWeaponSocket";
	}

	BasicDamage = CharacterParameters.Damage;
	AttackSpeed = CharacterParameters.AttackSpeed;
	HealthComponent->SetMaxHealth(CharacterParameters.MaxHealth * CharacterLevel);
}

void ASideCharacterBase::DestroyCharacter()
{
	
}


