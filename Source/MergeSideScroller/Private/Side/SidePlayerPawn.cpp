// Fill out your copyright notice in the Description page of Project Settings.


#include "Side/SidePlayerPawn.h"

// Sets default values
ASidePlayerPawn::ASidePlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASidePlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASidePlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ASidePlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ASidePlayerPawn::SetPlayerCharacter(ASideCharacterBase* PlayerCharacter_In)
{
	PlayerCharacter = PlayerCharacter_In;
}

