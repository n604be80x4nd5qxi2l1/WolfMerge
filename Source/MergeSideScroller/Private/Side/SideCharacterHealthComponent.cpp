// Fill out your copyright notice in the Description page of Project Settings.


#include "Side/SideCharacterHealthComponent.h"

// Sets default values for this component's properties
USideCharacterHealthComponent::USideCharacterHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;



	// ...
	MaxHealth = 0.f;
	CurrentHealth = 0.f;
	RegenRate = 1.0f;
	//OwnerCharacter = nullptr;
}


// Called when the game starts
void USideCharacterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	CurrentHealth = MaxHealth;

	//auto Owner = GetOwner();

	//OwnerCharacter = Cast<ASideCharacterBase>(Owner);
	//if(OwnerCharacter)
	//{
		StartRegen();
	//};


	
}


// Called every frame
void USideCharacterHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void USideCharacterHealthComponent::DealDamage(float IncomingDamage)
{
	CurrentHealth -= IncomingDamage;
}

void USideCharacterHealthComponent::DoDefaultHealing()
{
	float HealingAmount = MaxHealth / 2;
	if (CurrentHealth + HealingAmount > MaxHealth)
	{
		CurrentHealth = MaxHealth;
	}
}

void USideCharacterHealthComponent::BoostRegen()
{
	RegenRate/=6;
	StartRegen();
}

float USideCharacterHealthComponent::GetCurrentHealth()
{
	return CurrentHealth;
}

float USideCharacterHealthComponent::GetMaxHealth()
{
	return MaxHealth;
}

void USideCharacterHealthComponent::SetMaxHealth(float InMaxHealth)
{
	MaxHealth = InMaxHealth;
	CurrentHealth = MaxHealth;
}

void USideCharacterHealthComponent::StartRegen()
{
	if (TimerHandle.IsValid()) TimerHandle.Invalidate();
	//if (OwnerCharacter->IsEnemy) return;
	
	GetWorld()->GetTimerManager().SetTimer(
		TimerHandle,
		this,
		&USideCharacterHealthComponent::RegenTick,
		RegenRate,
		false);
}

void USideCharacterHealthComponent::RegenTick()
{
	if (CurrentHealth < MaxHealth)
	{
		CurrentHealth++;
	}
	else
	{
		CurrentHealth = MaxHealth;
		TimerHandle.Invalidate();
	}
}

