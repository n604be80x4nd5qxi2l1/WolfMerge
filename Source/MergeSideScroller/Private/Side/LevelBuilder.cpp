// Fill out your copyright notice in the Description page of Project Settings.


#include "Side/LevelBuilder.h"

#include <Kismet/GameplayStatics.h>


// Sets default values
ALevelBuilder::ALevelBuilder()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	LevelPartDefaultOffset = -4800.0f;
	PartsAmount = 3;
	CurrentLevelPart = nullptr;
	LevelSettings = FSideLevelSettings();

}

// Called when the game starts or when spawned
void ALevelBuilder::BeginPlay()
{
	Super::BeginPlay();

	MakeLevel();
}

// Called every frame
void ALevelBuilder::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALevelBuilder::Initialize(const uint8& InPartsAmount, const FSideLevelSettings& InLevelSettings)
{
	PartsAmount = InPartsAmount;
	LevelSettings = InLevelSettings;
}

void ALevelBuilder::MakeLevel()
{
	if (PartsAmount < 3)
		PartsAmount = 3;

	for (uint8 i = 0; i < PartsAmount; i++)
	{
		float CurrentPartOffset = i * LevelPartDefaultOffset;
		UClass* LevelPartToSpawn;
		
		if (i == 0)
		{
			if (IsValid(LevelSettings.LevelPartBegin))
				LevelPartToSpawn = LevelSettings.LevelPartBegin;
			else continue;
		}
		else if (i == PartsAmount-1)
		{
			if (IsValid(LevelSettings.LevelPartEnd))
				LevelPartToSpawn = LevelSettings.LevelPartEnd;
			else continue;
		}
		else if (LevelSettings.LevelParts.Num() > 0)
		{
			int32 CurrentPartIndex = 0;
			if (LevelSettings.LevelParts.Num() > 1)
				CurrentPartIndex = FMath::RandRange(0, LevelSettings.LevelParts.Num() - 1);

			if (IsValid(LevelSettings.LevelParts[CurrentPartIndex]))
				LevelPartToSpawn = LevelSettings.LevelParts[CurrentPartIndex];
			else continue;
		}
		else continue;
		
		CurrentLevelPart = SpawnLevelPart(LevelPartToSpawn, CurrentPartOffset);

		if (i == 0)
		{
			SpawnPlayer();
		}
		else if (i < PartsAmount-1)
		{
			SpawnCharacterInLocation({280,(CurrentPartOffset+(LevelPartDefaultOffset/4)),100}, true);
			SpawnCharacterInLocation({280,(CurrentPartOffset-(LevelPartDefaultOffset/4)),100}, true);
		}
		else
		{
			SpawnCharacterInLocation({280,CurrentPartOffset,100}, true, true);
		}
		
	}
}

void ALevelBuilder::SpawnPlayer()
{
	FVector SpawnLocation = {280,0,100};
	FActorSpawnParameters SpawnParameters;
	ASidePlayerPawn* PlayerGameBoard = GetWorld()->SpawnActor<ASidePlayerPawn>(PlayerGameBoardClass, SpawnParameters);
	UGameplayStatics::GetPlayerController(GetWorld(),0)->Possess(Cast<APawn>(PlayerGameBoard));
	
	ASideCharacterBase* PlayerCharacter = SpawnCharacterInLocation({280,0,100}, false);

	FAttachmentTransformRules AttachmentRules{EAttachmentRule::KeepRelative,false};
	PlayerGameBoard->AttachToActor(PlayerCharacter, AttachmentRules);
	PlayerGameBoard->SetPlayerCharacter(PlayerCharacter);

	
}

ASideCharacterBase* ALevelBuilder::SpawnCharacterInLocation(FVector Location, bool IsEnemy, bool IsBoss)
{
	
	FName CurrentCharacterName;

	if (IsEnemy && !IsBoss)
	{
		uint32 RegularEnemiesNum = LevelSettings.RegularEnemyNames.Num();
		if (RegularEnemiesNum > 0)
		{
				
			if (RegularEnemiesNum > 1)
			{
				auto CurrentCharacterNameIndex = FMath::RandRange(0, RegularEnemiesNum-1);
				if (LevelSettings.RegularEnemyNames[CurrentCharacterNameIndex].GetStringLength() > 0)
				{
					CurrentCharacterName = LevelSettings.RegularEnemyNames[CurrentCharacterNameIndex];
				}
				
			}
			else if (LevelSettings.RegularEnemyNames[0].GetStringLength() > 0)
			{
				CurrentCharacterName = LevelSettings.RegularEnemyNames[0];
			}
			else return nullptr;
		}
		else return nullptr;
	}
	
	else if (IsEnemy && IsBoss)
	{
		if (LevelSettings.BossEnemyName.GetStringLength() > 0)
		{
			CurrentCharacterName = LevelSettings.BossEnemyName;
		}
		else return nullptr;
	}

	else
	{
		CurrentCharacterName = "PlayerWolf";
	}
	
	//Change to basic SpawnActor, let Actor get his parameters himself; 	
	/*FRotator Rotation(0.0f, 0.0f, 0.0f);
	FTransform SpawnTransform = FTransform(Rotation, Location);
	const auto NewCharacter =
		World->SpawnActorDeferred<ASideCharacterBase>(
			CurrentCharacterParameters->CharacterClass,
			SpawnTransform,
			nullptr, nullptr,
			ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn
			);
	NewCharacter->Initialize(*CurrentCharacterParameters, IsEnemy);
	NewCharacter->FinishSpawning(SpawnTransform);*/
	
	ASideCharacterBase* NewCharacter =
		GetWorld()->SpawnActor<ASideCharacterBase>(
			ASideCharacterBase::StaticClass());

	FString ContextString;
	FCharacterParameters* CurrentCharacterParameters =
		CharactersDataTable->FindRow<FCharacterParameters>(CurrentCharacterName, ContextString);
	NewCharacter->Initialize(*CurrentCharacterParameters, IsEnemy);


	return NewCharacter;

}

AActor* ALevelBuilder::SpawnLevelPart(UClass* LevelPartClass, const float& SpawnOffset) const
{
	const FVector SpawnTransformTranslation{0, SpawnOffset, 0};
	const FTransform SpawnTransform{SpawnTransformTranslation};
	const FActorSpawnParameters SpawnParameters;
	
	AActor* NewLevelPart = GetWorld()->SpawnActor(LevelPartClass, &SpawnTransform, SpawnParameters);
	return NewLevelPart;	
}

