// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Types.h"
#include "SideCharacterHealthComponent.h"
#include "SideCharacterWeaponBase.h"
#include "SideCharacterBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FCharacterDie);

UCLASS()
class MERGESIDESCROLLER_API ASideCharacterBase : public ACharacter
{
	GENERATED_BODY()
private:
	FName MainWeaponSocketName;
	FName SecondaryWeaponSocketName;
	
public:
	// Sets default values for this character's properties
	ASideCharacterBase();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Basic Parameters")
	bool bIsEnemy;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Basic Parameters")
	int CharacterLevel;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Combat")
	float BasicDamage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="Combat")
	float AttackSpeed;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	FName CharacterName = "Default";
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	FCharacterParameters CharacterParameters;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="")
	TSubclassOf<ASideCharacterWeaponBase> CharacterWeaponClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	ASideCharacterWeaponBase* CharacterWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category="Components")
	USideCharacterHealthComponent* HealthComponent;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components")
	UParticleSystemComponent* CharacterAuraBasicComponent = nullptr;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category="Components")
	UParticleSystemComponent* CharacterAuraElementComponent = nullptr;

	UPROPERTY(BlueprintCallable, BlueprintAssignable, Category="Character")
	FCharacterDie OnCharacterDie;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//On spawn initialization
	void Initialize(const FCharacterParameters InCharacterParameters, const bool InIsEnemy);

	
	UFUNCTION(BlueprintCallable)
	void DestroyCharacter();
	
	
	

};
