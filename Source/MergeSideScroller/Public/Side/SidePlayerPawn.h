// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "SideCharacterBase.h"
#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "SidePlayerPawn.generated.h"

UCLASS()
class MERGESIDESCROLLER_API ASidePlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ASidePlayerPawn();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	ASideCharacterBase* PlayerCharacter;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void SetPlayerCharacter(ASideCharacterBase* PlayerCharacter_In);

};
