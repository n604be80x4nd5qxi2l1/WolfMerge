// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SideCharacterHealthComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class MERGESIDESCROLLER_API USideCharacterHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	USideCharacterHealthComponent();
	
	float MaxHealth;
	float CurrentHealth;
	float RegenRate;

	//UPROPERTY()
	//ASideCharacterBase* OwnerCharacter;

	FTimerHandle TimerHandle;
	

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
	void DealDamage(float IncomingDamage);

	UFUNCTION(BlueprintCallable)
	void DoDefaultHealing();
	
	UFUNCTION(BlueprintCallable)
	void BoostRegen();

	UFUNCTION(BlueprintCallable)
	float GetCurrentHealth();

	UFUNCTION(BlueprintCallable)
	float GetMaxHealth();

	UFUNCTION(BlueprintCallable)
	void SetMaxHealth(float InMaxHealth);
	
	void StartRegen();
	
	void RegenTick();

	
};
