// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/BoxComponent.h"
#include "GameFramework/Actor.h"
#include "LevelPartBasic.generated.h"

UCLASS()
class MERGESIDESCROLLER_API ALevelPartBasic : public AActor
{
	GENERATED_BODY()
	
public:

	/**
	 *	Box collisions on LevelPart
	 **/
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* EnterVolume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* FirstHalfVolume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* CenterVolume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* SecondHalfVolume;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UBoxComponent* ExitVolume;
	
	// Sets default values for this actor's properties
	ALevelPartBasic();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
