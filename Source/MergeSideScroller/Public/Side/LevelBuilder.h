// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Types.h"
#include "Side/SideCharacterBase.h"
#include "Side/SidePlayerPawn.h"
#include "GameFramework/Actor.h"
#include "LevelBuilder.generated.h"

UCLASS()
class MERGESIDESCROLLER_API ALevelBuilder : public AActor
{
	GENERATED_BODY()
	
public:	


	
	uint8 PartsAmount;

	UPROPERTY()
	AActor* CurrentLevelPart;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	UClass* PlayerGameBoardClass;
	
	FSideLevelSettings LevelSettings;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	UDataTable* CharactersDataTable;


	// Sets default values for this actor's properties
	ALevelBuilder();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	void Initialize(const uint8& InPartsAmount, const FSideLevelSettings& InLevelSettings);

	UFUNCTION(BlueprintCallable)
	ASideCharacterBase* SpawnCharacterInLocation(FVector Location, bool IsEnemy, bool IsBoss = false);

	void MakeLevel();

	void SpawnPlayer();

	UFUNCTION(BlueprintCallable)
	AActor* SpawnLevelPart(UClass* LevelPartClass, const float& SpawnOffset) const;

private:
	float LevelPartDefaultOffset;
};
