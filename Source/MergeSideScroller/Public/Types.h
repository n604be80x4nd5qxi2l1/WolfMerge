// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "Types.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EElement : uint8
{
	Fire,
	Lightning
};

USTRUCT(BlueprintType)
struct FSideLevelSettings : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemies")
	TArray<FName> RegularEnemyNames;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Enemies")
	FName BossEnemyName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Parts")
	TArray<UClass*> LevelParts;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Parts")
	UClass* LevelPartBegin;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Level Parts")
	UClass* LevelPartEnd;
	
};

USTRUCT(BlueprintType)
struct FSoundsLibrary : public FTableRowBase
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* FireHitSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* LightningHitSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* ReflectedHitSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* FootstepSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* PotionUseSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* BurningSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "General Sounds")
	USoundBase* DeathEventSoundBase;
};

USTRUCT(BlueprintType)
struct FCharacterSounds : public FTableRowBase
{
	GENERATED_BODY()
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Sounds")
	USoundBase* WeaponSwingSoundBase;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Sounds")
	USoundBase* WeaponHitSoundBase;
};


USTRUCT(BlueprintType)
struct FCharacterParameters : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Parameters")
	bool RightHanded;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Parameters")
	float Damage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Parameters")
	float MaxHealth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Parameters")
	float AttackSpeed;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Element")
	EElement CharacterElement;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Class")
	UClass* CharacterClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character Sounds")
	FCharacterSounds CharacterSounds;
	
};





UCLASS()
class MERGESIDESCROLLER_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
	
};
