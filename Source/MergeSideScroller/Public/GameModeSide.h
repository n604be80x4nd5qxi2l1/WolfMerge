// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include <Engine/DataTable.h>

#include "CoreMinimal.h"
#include "Side/LevelBuilder.h"
#include "GameFramework/GameModeBase.h"
#include "GameModeSide.generated.h"

/**
 * 
 */
UCLASS()
class MERGESIDESCROLLER_API AGameModeSide : public AGameModeBase
{
	GENERATED_BODY()

public:
	FString Options;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	UDataTable* LevelSettingsDataTable;

	FSideLevelSettings* CurrentLevelSettings;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="")
	UClass* LevelBuilderClass;

	UPROPERTY()
	ALevelBuilder* LevelBuilder;
	
	bool IsFirstSide;
	bool IsLastSide;		
	
	virtual void BeginPlay() override;
	FString GetOptionValue(const FString& Key) const;
};
